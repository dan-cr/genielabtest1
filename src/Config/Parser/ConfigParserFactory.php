<?php

declare (strict_types = 1);

namespace Thegenielabtest\Config\Parser;

class ConfigParserFactory {

    /**
     * Disable instantiation of this class
     */
    private function __construct() {}

    /**
     * Extend this to add new parsers to avoid violating open-closed principle
     *
     * @return array
     */
    protected static function parsers() {
        return [
            'default' => DefaultConfigParser::class,
            'yaml'    => YamlConfigParser::class
        ];
    }

    /**
     * Get parser instance by extension
     *
     * @param string $parser
     * @return mixed
     */
    public static function getParser(string $parser) {

        $availableParsers = self::parsers();

        /*
         * Default to default parser if parser is no such parser exists
         */
        if (array_key_exists($parser, $availableParsers)) {
            $parserClass = $availableParsers[$parser];
        } else {
            $parserClass = $availableParsers['default'];
        }

        $parserInstance = new $parserClass();

        /*
         * Return the parser instance only if the class implements the config parser interface
         */
        if ($parserInstance instanceof InterfaceConfigParser) {
            return $parserInstance;
        }

        throw new \Exception("The {$parserClass} class does not implement the InterfaceConfigParser interface");
    }

}
