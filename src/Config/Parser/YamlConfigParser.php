<?php

declare (strict_types = 1);

namespace Thegenielabtest\Config\Parser;

use Symfony\Component\Yaml\Yaml;

final class YamlConfigParser implements InterfaceConfigParser {

    /**
     * Parse Yaml files
     *
     * @param string $configPath
     * @return mixed
     */
    public function parse(string $configPath) {
        return Yaml::parseFile($configPath);
    }
}
