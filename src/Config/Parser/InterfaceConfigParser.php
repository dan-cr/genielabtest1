<?php

declare(strict_types=1);

namespace Thegenielabtest\Config\Parser;

interface InterfaceConfigParser {
    public function parse(string $configPath);
}