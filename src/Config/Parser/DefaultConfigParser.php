<?php

declare(strict_types=1);

namespace Thegenielabtest\Config\Parser;
use Symfony\Component\Yaml\Yaml;

final class DefaultConfigParser implements InterfaceConfigParser {
    
    /**
     * Parse a random file
     *
     * @param string $configPath
     * @return string
     */
    public function parse(string $configPath) {
        return [];
    }
}