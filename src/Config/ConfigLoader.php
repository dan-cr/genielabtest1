<?php

declare (strict_types = 1);

namespace Thegenielabtest\Config;

use Exception;
use Thegenielabtest\Config\Parser\ConfigParserFactory;
use Thegenielabtest\Exceptions\InvalidPathException;

class ConfigLoader {

    private $configPath = null;

    private $configFiles = [];

    /**
     * Set config path which is to be searched
     *
     * @param string $path
     * @return ConfigLoader
     */
    public function setConfigPath(string $path) {
        $this->configPath = $this->addTrailingSlashIfMissing($path);

        return $this;
    }

    /**
     * Get config files by extension
     *
     * @param string $extension
     * @return mixed
     */
    public function getConfigFiles(string $extension = "yaml") {

        if (empty($this->configPath) || !is_dir($this->configPath)) {
            throw new InvalidPathException("The specified config path doesn't exist");
        }

        $files = glob($this->configPath . "*." . $extension);

        $this->configFiles[$extension] = $files;

        return $this->configFiles;
    }

    private function addTrailingSlashIfMissing(string $path) {
        return rtrim($path, "/") . '/';
    }

    /**
     * Iterate through config files which are then parsed into memory
     *
     * @todo Simplify this method as it's doing too much
     * @return void
     */
    public function parseAndStoreConfig() {

        // Merged config values
        $config = array();

        foreach ($this->getConfigFiles() as $extension => $configFiles) {

            try {
                $configParser = ConfigParserFactory::getParser($extension);
            } catch (\Exception $e) {
                // TODO: Do something more useful with exceptions
                die("Parser instantiation error: " . $e->getMessage());
            }

            foreach ($configFiles as $file) {
                $data   = $configParser->parse($file);
                $config = array_merge($config, $data);
            }

        }

        /*
         * Read parsed config values into memory / cache
         */
        foreach ($config as $key => $value) {
            Config::set($key, $value);
        }

    }

}
