<?php

declare(strict_types=1);

namespace Thegenielabtest\Config;

class Config {

    /**
     * Static config array holding containing parsed values
     *
     * @var array
     */
    protected static array $config = array();

    /**
     * Disable instantiation for this config
     */
    private function __construct() {}

    /**
     * Set config value
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public static function set(string $key, $value) {
        self::$config[$key] = $value;
    }

    /**
     * Get config value by key
     *
     * @param string $key
     * @return void
     */
    public static function get(string $key) {
        return self::$config[$key] ?? NULL;
    }

    /**
     * Return entire config array
     *
     * @return array $config
     */
    public static function all() {
        return self::$config;
    }

}