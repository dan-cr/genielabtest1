<?php

declare (strict_types = 1);

namespace Thegenielabtest\Routes;

use Thegenielabtest\Http\Controllers\JotformController;
use Thegenielabtest\Http\Controllers\ShopifyController;
use Thegenielabtest\Http\Request;
use Thegenielabtest\Http\Router;

return [

    // Shopify Install Route
    [
        Router::HTTP_METHOD  => Request::HTTP_GET,
        Router::HTTP_PATH    => "/",
        Router::HTTP_HANDLER => ShopifyController::class,
        Router::HTTP_ACTION  => "install"
    ],

    // Shopify Callback Route
    [
        Router::HTTP_METHOD  => Request::HTTP_GET,
        Router::HTTP_PATH    => "/shopify/auth/callback",
        Router::HTTP_HANDLER => ShopifyController::class,
        Router::HTTP_ACTION  => "authCallback"
    ],

    // Jotform Webhook handler
    [
        Router::HTTP_METHOD  => Request::HTTP_POST,
        Router::HTTP_PATH    => "/jotformwebhook",
        Router::HTTP_HANDLER => jotformController::class,
        Router::HTTP_ACTION  => "handler"
    ]
];
