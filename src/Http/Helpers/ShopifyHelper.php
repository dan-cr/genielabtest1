<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http\Helpers;

class ShopifyHelper {

    /**
     * Check if the shop domain is valid and in the correct format
     *
     * @param string $shop
     * @return bool
     */
    public static function validateShopDomain(string $shop) {
        $substring = explode('.', $shop);

        // 'example.myshopify.com'
        if (count($substring) != 3) {
            return false;
        }

        // allow dashes and alphanumberic characters
        $substring[0] = str_replace('-', '', $substring[0]);

        return (ctype_alnum($substring[0]) && $substring[1] . '.' . $substring[2] == 'myshopify.com');
    }

    /**
     * Validate HMAC params
     *
     * @param array $params
     * @param string $secret
     * @return bool
     */
    public static function validateHmac(array $params, string $secret) {
        $hmac = $params['hmac'];
        unset($params['hmac']);
        ksort($params);

        $computedHmac = hash_hmac('sha256', http_build_query($params), $secret);

        return hash_equals($hmac, $computedHmac);
    }

    /**
     * Helper for exchanging shop credentials
     *
     * @param string $shop
     * @param string $apiKey
     * @param string $secret
     * @param string $code
     * @return mixed
     */
    public static function getAccessToken($shop, $apiKey, $secret, $code) {
        $query = array(
            'client_id'     => $apiKey,
            'client_secret' => $secret,
            'code'          => $code
        );

        // Build access token URL
        $access_token_url = "https://{$shop}/admin/oauth/access_token";

        // Configure curl client and execute request
        $curl        = curl_init();
        $curlOptions = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $access_token_url,
            CURLOPT_POSTFIELDS     => http_build_query($query)
        );
        curl_setopt_array($curl, $curlOptions);
        $jsonResponse = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return $jsonResponse['access_token'];
    }

    /**
     * Make a request to shopify
     *
     * @param [type] $shop
     * @param [type] $token
     * @param [type] $resource
     * @param array $params
     * @param string $method
     * @return void
     */
    public static function performShopifyRequest($shop, $token, $resource, $params = array(), $method = 'GET') {
        $url = "https://{$shop}/admin/api/2021-10/{$resource}.json";

        $curlOptions = array(
            CURLOPT_RETURNTRANSFER => true
        );

        if ($method == 'GET') {
            if (!is_null($params)) {
                $url = $url . "?" . http_build_query($params);
            }
        } else {
            $curlOptions[CURLOPT_CUSTOMREQUEST] = $method;
        }

        $curlOptions[CURLOPT_URL] = $url;

        $requestHeaders = array(
            "X-Shopify-Access-Token: ${token}",
            "Accept: application/json"
        );

        if ($method == 'POST' || $method == 'PUT') {
            $requestHeaders[] = "Content-Type: application/json";

            if (!is_null($params)) {
                $curlOptions[CURLOPT_POSTFIELDS] = json_encode($params);
            }
        }

        $curlOptions[CURLOPT_HTTPHEADER] = $requestHeaders;

        $curl = curl_init();
        curl_setopt_array($curl, $curlOptions);
        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);
    }

}
