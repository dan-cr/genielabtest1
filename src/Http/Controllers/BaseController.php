<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http\Controllers;

use Thegenielabtest\Repositories\ShopRepository;

use Thegenielabtest\Database\DB;

class BaseController {

    protected DB $db;

    /**
     * Ugly... should probably be using a DI container, but this is only a test so it doesn't really matter :)
     *
     * @param DB $db
     * @return void
     */
    public function setDatabase(DB $db) {   
        $this->db = $db;
    }
    
}
