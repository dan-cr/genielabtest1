<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http\Controllers;

use Thegenielabtest\Config\Config;
use Thegenielabtest\Entities\Shop;
use Thegenielabtest\Http\Helpers\ShopifyHelper;
use Thegenielabtest\Http\Request;
use Thegenielabtest\Repositories\ShopRepository;

class ShopifyController extends BaseController {

    public function install() {

        // App API Key + secret
        $apiKey    = Config::get('shopify')['api_secret'];
        $apiSecret = Config::get('shopify')['shared_secret'];

        // Shopify OAuth app scope
        $scope = "read_customers,write_customers";

        // Shop URL / Name
        $shopUrl = Request::get('shop');

        if (!$shopUrl) {
            throw new \Exception('Missing shop url');
        }

        if (!ShopifyHelper::validateShopDomain($shopUrl)) {
            throw new \Exception('Invalid shop url');
        }

        $redirectUri = Request::buildUri('/shopify/auth/callback');

        $installUrl = "https://{$shopUrl}/admin/oauth/authorize?client_id={$apiKey}&scope={$scope}&redirect_uri={$redirectUri}";

        $shop = (new ShopRepository($this->db))->findByName($shopUrl);

        header("Location: " . $installUrl);
    }

    public function authCallback() {

        // Retrieve url params from callback url
        $params = Request::get();
        $shop   = Request::get('shop');
        $code   = Request::get('code');

        // App API Key + secret
        $apiKey    = Config::get('shopify')['api_secret'];
        $apiSecret = Config::get('shopify')['shared_secret'];

        // Validate shop domain and hmac
        $validShop = ShopifyHelper::validateShopDomain($shop);
        $validHmac = ShopifyHelper::validateHmac($params, $apiSecret);

        // Generate an access token from params
        $token = ShopifyHelper::getAccessToken($shop, $apiKey, $apiSecret, $code);

        $shopData = [
            'name'      => $shop,
            'token'     => $token,
            'activated' => 1
        ];

        // Create shop in DB
        (new Shop($this->db))->create($shopData);

        // Return to shopify URL after auth callback
        header("Location: " . "https://" . $shop);

    }

}
