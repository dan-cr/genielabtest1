<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http\Controllers;

use Thegenielabtest\Http\Helpers\ShopifyHelper;
use Thegenielabtest\Http\Request;
use Thegenielabtest\Repositories\ShopRepository;

class JotformController extends BaseController {

    public function handler() {

        $result = $_REQUEST['rawRequest'];
        $obj    = json_decode($result, true);

        $first    = $obj['q3_fullName3']['first'];
        $last     = $obj['q3_fullName3']['last'];
        $address  = $obj['q4_address4']['addr_line1'];
        $city     = $obj['q4_address4']['city'];
        $state    = $obj['q4_address4']['state'];
        $postcode = $obj['q4_address4']['postal'];
        $phone    = str_replace("-", "", $obj['q5_phoneNumber5']['full']);
        $email    = $obj['q6_email6'];
        $shopName = $obj['q16_shop1'];

        $shop = (new ShopRepository($this->db))->findByName($shopName);

        $access_token = $shop->getToken();

        $response = ShopifyHelper::performShopifyRequest($shopName, $access_token, 'customers', [

            'customer' => [
                'first_name' => $first,
                'last_name'  => $last,
                'email'      => $email,
                'addresses'  => [
                    [
                        'address1' => $address,
                        'city'     => $city,
                        'zip'      => $postcode
                    ]
                ]
            ]

        ], Request::HTTP_POST);

        error_log(print_r($response, true));
    }

}
