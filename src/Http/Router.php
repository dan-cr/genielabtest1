<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http;

use FastRoute;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Thegenielabtest\Config\Config;
use Thegenielabtest\Database\DB;
use Thegenielabtest\Http\Request;
use Thegenielabtest\Http\RequestInterface;

class Router {

    /**
     * Seperator for class + method handler
     */
    const HANDLER_DELIMITER = "@";

    /**
     * Route constants
     */
    const HTTP_METHOD  = "method";
    const HTTP_HANDLER = "handler";
    const HTTP_PATH    = "path";
    const HTTP_ACTION  = "action";

    /**
     * @var RequestInterface
     */
    private $request;

    private $routes = array();

    private $db;

    public function __construct(RequestInterface $request, DB $db) {
        $this->request = $request;
        $this->db      = $db;
    }

    /**
     * Adds a route to be matched and dispatched
     *
     * @param string $method
     * @param string $path
     * @param string $handler
     * @param string $method
     * @return void
     */
    public function addRoute(string $method, string $path, string $handler, string $action) {

        if (!in_array($method, $this->allowedMethods())) {
            throw new exception("Method not supported");
        }

        $this->routes[] = [
            "method"  => $method,
            "path"    => $path,
            "handler" => $handler,
            "action"  => $action
        ];
    }

    /**
     * Add all routes to array
     *
     * @return void
     */
    public function addRoutes() {

        $routes = $this->getRouteConfig();

        foreach ($routes as $route) {
            $this->addRoute(
                $route[static::HTTP_METHOD],
                $route[static::HTTP_PATH],
                $route[static::HTTP_HANDLER],
                $route[static::HTTP_ACTION]
            );
        }

    }

    /**
     * Allowed HTTP verbs in route config
     *
     * @return array
     */
    private function allowedMethods() {
        return [
            Request::HTTP_GET,
            Request::HTTP_POST,
            Request::HTTP_DELETE,
            Request::HTTP_PATCH
        ];
    }

    /**
     * Get route config array
     *
     * @return array
     */
    private function getRouteConfig() {
        return require_once dirname(__DIR__, 1) . '/Routes/Routes.php';
    }

    /**
     * @return Dispatcher
     */
    private function getDispatcher(): Dispatcher {
        $dispatcher = \FastRoute\simpleDispatcher(function (RouteCollector $r) {
            foreach ($this->routes as $route) {
                $r->addRoute(
                    $route[static::HTTP_METHOD],
                    $route[static::HTTP_PATH],
                    $route[static::HTTP_HANDLER] . static::HANDLER_DELIMITER . $route[static::HTTP_ACTION]
                );
            }
        });

        return $dispatcher;
    }

    /**
     * Retrieves a URI that is compatible with the route dispatcher excluding the query string and base project dir
     *
     * @return string $uri
     */
    private function getFormattedURI() {

        $base_uri = Config::get('project_base_directory') ?? '/';

        // Get raw request URI
        $uri = str_replace($base_uri, "", $this->request->getUri());

        // Remove query string params prior to dispatching
        $uri = $this->stripQueryString($uri);

        // Decode URL-encoded strings
        $uri = rawurldecode($uri);

        return $uri;
    }

    /**
     * Remove query string from some URI
     *
     * @param string $uri
     * @return void
     */
    private function stripQueryString(string $uri) {
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        return $uri;
    }

    /**
     * Attempt to dispatch using provided request URI and method
     *
     * @param string $requestMethod
     * @param string $requestUri
     * @param Dispatcher $dispatcher
     * @return void
     */
    private function dispatch(string $requestMethod, string $requestUri, Dispatcher $dispatcher) {

        $routeInfo = $dispatcher->dispatch($requestMethod, $requestUri);

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                die("not found");
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                die("method not allowed");
                break;
            case Dispatcher::FOUND:
                list($state, $handler, $vars) = $routeInfo;
                list($class, $method)         = explode(static::HANDLER_DELIMITER, $handler, 2);

                $controller = new $class();
                $controller->setDatabase($this->db);
                $controller->{$method}(...array_values($vars));

                unset($state);
                break;
        }
    }

    /**
     * @return RequestInterface
     */
    private function getRequest(): RequestInterface {
        return $this->request;
    }

    /**
     * Begin route dispatch process
     *
     * @return void
     */
    public function execute() {
        $this->addRoutes();
        $this->dispatch(
            $this->request->getMethod(),
            $this->getFormattedURI(),
            $this->getDispatcher()
        );
    }
}
