<?php

declare (strict_types = 1);

namespace Thegenielabtest\Http;

use Thegenielabtest\Config\Config;

class Request implements RequestInterface {

    // HTTP Verbs
    const HTTP_GET    = "GET";
    const HTTP_POST   = "POST";
    const HTTP_DELETE = "DELETE";
    const HTTP_PATCH  = "PATCH";

    const METHOD = 'REQUEST_METHOD';
    const URI    = 'REQUEST_URI';

    /**
     * @return string
     */
    public function getMethod(): string {
        return $_SERVER[static::METHOD];
    }

    /**
     * @return string
     */
    public function getUri(): string {
        return $_SERVER[static::URI];
    }

    /**
     * Retrieve GET parameters by key or all
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key = null) {
        if ($key) {
            return $_GET[$key] ?? null;
        }
        return $_GET;
    }

    /**
     * Retrieve POST parameters by key or all
     *
     * @param string $key
     * @return mixed
     */
    public static function post($key = null) {
        if ($key) {
            return $_POST[$key] ?? null;
        }
        return $_POST;
    }

    public static function buildUri(string $path) {
        return self::getProtocol() . $_SERVER['SERVER_NAME'] . Config::get('project_base_directory') . $path;
    }

    public static function getProtocol() {
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }
        return $protocol;
    }

}
