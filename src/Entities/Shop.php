<?php

declare (strict_types = 1);

namespace Thegenielabtest\Entities;

use Thegenielabtest\Database\DB;

class Shop {

    private $id;

    private $name;

    private $token;

    private $activated;

    protected $db;

    /**
     * Ugly... should probably be using a DI container, but this is only a test so it doesn't really matter :)
     *
     * @param mixed $db
     * @return void
     */
    public function __construct($db = null) {
        $this->db = $db;
    }

    /**
     * Getter Methods
     */
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getToken() {
        return $this->token;
    }

    public function getActivated() {
        return $this->activated;
    }

    /**
     * Setter methods
     *
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setToken($token) {
        $this->token = $token;
        return $this;
    }

    public function setActivated($activated) {
        $this->activated = $activated;
        return $this;
    }

    public function persist() {
        $this->db->run("UPDATE shops SET id = ?, name = ?, token = ?, activated = ? WHERE id = ?",
            [
                $this->getId(),
                $this->getName(),
                $this->getToken(),
                $this->getActivated(),
                $this->getId()
            ]);

        return $this;
    }

    public function create($data) {
        $this->db->run("INSERT INTO shops (name, token, activated) VALUES(?, ?, ?)",
            [
                $data['name'],
                $data['token'],
                $data['activated']
            ]
        );
    }

}
