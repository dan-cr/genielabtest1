<?php

declare (strict_types = 1);

use Thegenielabtest\Config\Config;
use Thegenielabtest\Config\ConfigLoader;
use Thegenielabtest\Http\Router;
use Thegenielabtest\Routes\Routes;
use Thegenielabtest\Http\Request;
use Thegenielabtest\Database\DB;

require_once __DIR__ . '/../vendor/autoload.php';

$configLoader = (new ConfigLoader())
    ->setConfigPath(dirname(__DIR__, 1))
    ->parseAndStoreConfig();

$db = new DB(
    Config::get('database')['name'],
    Config::get('database')['username'],
    Config::get('database')['password'],
    Config::get('database')['host'],
    Config::get('database')['port'],
);

$request = new Request();
$router = (new Router($request, $db))->execute();