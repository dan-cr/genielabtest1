<?php

declare (strict_types = 1);

namespace Thegenielabtest\Repositories;

use Thegenielabtest\Database\DB;
use Thegenielabtest\Entities\Shop;

class ShopRepository {

    private $db;

    public function __construct(DB $db) {
        $this->db = $db;
    }

    /**
     * Find a shop by its id
     *
     * @param integer|null $id
     * @return Shop
     */
    public function findById(int $id = null) {
        $shop = $this->db->run("SELECT * FROM shops where id = ?", [$id])->fetch();
        return ($shop) ? $this->build($shop) : null;
    }

    /**
     * Find a shop by its name
     *
     * @param string|null $name
     * @return Shop
     */
    public function findByName(string $name = null) {
        $shop = $this->db->run("SELECT * FROM shops where name = ?", [$name])->fetch();
        return ($shop) ? $this->build($shop) : null;
    }

    /**
     * Construct a DTO / Entity object from the query
     *
     * @param [type] $data
     * @return void
     */
    public function build($data) {

        $shop = new Shop($this->db);

        foreach ($data as $property => $value) {
            $property = "set" . ucfirst($property);
            $shop->{$property}($value);
        }

        return $shop;
    }

}
